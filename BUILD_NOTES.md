things to install
* curl
* less
* MBS & deps
* fedmod
* make
* postgresql (client) -- actually let's instlal in the lab

student machine services
* MBS
* Openshift

shared services
* clone of repos
* gitforge

lab should be clone to a directory ~/appstream-lab


core images:
```
core.example.com:5000/appstream-lab/fedmod-container
core.example.com:5000/appstream-lab/fedpkg-container
core.example.com:5000/appstream-lab/mbs-container
core.example.com:5000/postgresql-96-rhel7
core.example.com:5000/rhel8
registry.fedoraproject.org/fedora-minimal

```
