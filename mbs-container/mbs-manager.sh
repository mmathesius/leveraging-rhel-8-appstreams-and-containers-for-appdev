#!/bin/bash

WDIR=${WORKDIR:-$PWD}
RDIR=${RPMDIR:-$PWD/rpms}
MBDIR=${MODBUILDDIR:-$WDIR/modulebuild}

mkdir -p $WDIR/modulebuild && \
podman run -it --rm \
    --tmpfs /tmp \
    --tmpfs /run \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    --privileged \
    -v "$WDIR":/wd \
    -v "$MBDIR":/root/modulebuild \
    -v "$RDIR":/root/rpms \
    --workdir /wd \
    core.example.com:5000/appstream-lab/mbs-container "$@"
